---------------------------------------
420-523 -- EXAMEN FINAL -- Automne 2016
---------------------------------------

PROCÉDURE

- Faites un FORK de ce dépot en le renommant final-523-<votre prénom>.
- Donnez accès à l'usager ngorse.
- Faites un git clone de ce dépot sur votre ordinateur.
- Faites votre examen.
- Une fois votre examen terminé, assurez-vous que vous avez bien fait un commit de tous les fichiers nécessaires.


QUESTIONS
---------

Les questions se trouvent dans le fichier questions.pdf

